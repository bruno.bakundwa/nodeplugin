Node Plugin
-----------

A node zsh plugin for use with antigen derivates, adapted to work with Homebrew on OS X.

Features
--------

- Loads nvm.
- Sets up default node install (5.0.0)
- Loads and caches completions for node.

LICENSE
-------

[MIT](./LICENSE)

Credits
-------

The code is taken from [srijanshetty/node.plugin.zsh](https://github.com/srijanshetty/node.plugin.zsh)
