#
# Loads nvm and enables npm completion.
#	Adapted for use with homebrew, from node.plugin.zsh 
# (https://github.com/srijanshetty/node.plugin.zsh/blob/master/init.zsh)
#
#   Bruno Bakundwa <iam@corrni.com>
#

# is nvm installed?
brew list | grep nvm > /dev/null
if [[ $? == 0 ]]; then
	export NVM_DIR=~/.nvm
	source $(brew --prefix nvm)/nvm.sh
	nvm alias default 5.7.1 > /dev/null
fi

# Load NPM completion.
if (( $+commands[npm] )); then
  cache_file="${0:h}/cache.zsh"

  if [[ "$commands[npm]" -nt "$cache_file" || ! -s "$cache_file" ]]; then
    # npm is slow; cache its output.
    npm completion >! "$cache_file" 2> /dev/null
  fi

  source "$cache_file"

  unset cache_file
fi
